## microservice-users

Podemos arrancar + de 1 instancia (en distintos puertos -Dserver.port) para ver como se comporta el cluster

Para ello, si se quiere realizar en local, hay que escribir la propiedad:
eureka.instance.metadataMap.instanceId: ${spring.application.name}:${spring.application.instance_id:${server.port}}

De esta forma, partiendo de la misma aplicacion, aparecera como una nueva instancia de la aplicacion.


https://github.com/spring-cloud/spring-cloud-netflix/issues/63

```
dsyer commented on 12 Nov 2014
OK, I see what's happening. The sample apps have something like this:

eureka:
  instance:
    metadataMap:
      instanceId: ${spring.application.name}:${spring.application.instance_id:${server.port}}
I guess we need to maybe promote that to a default, but you can follow the same pattern to get your instances to show up as different IDs in Eureka.
```