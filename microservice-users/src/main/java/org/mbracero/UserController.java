package org.mbracero;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
	@RequestMapping("/users")
	public List<String> getUsers() {
		return Arrays.asList("user1", "user2", "user3");
	}
}