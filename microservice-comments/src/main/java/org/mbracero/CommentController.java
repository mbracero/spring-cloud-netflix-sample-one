package org.mbracero;

import java.util.Arrays;
import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CommentController {
	@RequestMapping("/comments")
	public List<String> getComments() {
		return Arrays.asList("hola", "hola mundo", "adios");
	}
}
